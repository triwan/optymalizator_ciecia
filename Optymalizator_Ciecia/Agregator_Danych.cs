﻿using System;
using System.Collections.Generic;

namespace Optymalizator_Ciecia
{
	public class Agregator_Danych
	{
		/*
		 * Zamienia dane na ciag znakow (string), uzywamy przed wyslaniem danych do programu nadrzednego
		 * do ktorego nie da sie ich przekazac wprost
		 */

		public Agregator_Danych ()
		{
		}

		private static String __CUTGLIB_cut_results(List<Dictionary<string, string>> data)
		{
			/*
			 * dane z kalkulatora (OutputSheetResults_by_Parts) przekazywane sa jako slowniki w liscie
			 *
			 * "
			 * OUTPUT CUTS RESULTS
				Cut=0:  X1=0;  Y1=1182;  X2=1800;  Y2=1182
				Cut=1:  X1=650;  Y1=0;  X2=650;  Y2=1182
				Cut=2:  X1=1300;  Y1=0;  X2=1300;  Y2=1182
			 * "
			 *
			 * Mamy jeszcze informacje o cieciach typu TRIM, ciecia formatek o napuchnietych bokach, klient nie placi za te ciecia
			 *
			 * dane o punktach do ciecia
			 *
			 * data = "[{ "trim": ... , "sheet": ... , "cut": ... , "X1": ... , "Y1": ... , "X2": ... , "Y2": ...  } , ... ]"
			 *
			 * callback z tej funkcji (string) = "{ "sheet": ... , "cut": ... , "X1": ... , "Y1": ... , "X2": ... , "Y2": ...  } , ..."
			 * sheet - nr arkusza (0-..X.), nr jest dodawany co 1 w zaleznosci od kolejnosci dodawania do kalkulatora
			 *
			 */

			const string cut_results_format = "{{\"sheet\":{0},\"cut\":{1},\"X1\":{2},\"Y1\":{3},\"X2\":{4},\"Y2\":{5},\"trim\":\"{6}\"}}";

			string callback = "";

			foreach (var dictionary in data)
			{
				String trim = char.ToUpper (dictionary ["trim"][0]) + dictionary ["trim"].Substring (1);  // z true/false na True/False
				callback += string.Format(cut_results_format,
					dictionary["sheet"],
					dictionary["cut"],
					dictionary["X1"],
					dictionary["Y1"],
					dictionary["X2"],
					dictionary["Y2"],
					trim
				);
				callback += ",";
			}

			callback = callback.Remove(callback.Length - 1);  // usuwa ostatni znak - ,

			return callback;
		}



		private static String __CUTGLIB_layout_results(List<Dictionary<string, string>> data, int ilosc_wykorzystanych_arkuszy)
		{
			/*
			 * informacje o punktach ciecia i formatkach
			 *
			 * "
			 * OUTPUT LAYOUT RESULTS
				Used 1 sheets
				Created 1 different layouts
				Layout=0:  Start Sheet=0;  Count of Sheet=1
				Sheet=0:  Width=1800; Height=2000
				Part=3; sheet=0; Width=650; Height=732; X=0; Y=0; Rotated=No
				Part=2; sheet=0; Width=650; Height=732; X=650; Y=0; Rotated=No
			 * "
			 *
			 * dane z kalkulatora (OutputSheetResults_by_Layout) przekazywane sa jako slowniki w liscie
			 * data = "[{ "part" : ... , "sheet" : ... , "part_width" : ..., part_height : ... , X: ... , Y: ... , rotated: ... , ilosc_resztek_z_arkusza: ...} , ... ]"
			 *
			 * callback z tej funkcji (string) = { "part" : ... , "sheet" : ... , "part_width" : ..., part_height : ... , X: ... , Y: ... , rotated: ... }, ...
			 *
			 * part - nr formatki (0-...), nr jest dodawany co 1 w zaleznosci od kolejnosci dodawania do kalkulatora
			 * rotated - czy mozna obracac formatke przy obliczeniu, jesli ma zachowac strukture to ten parametr musi byc false
			 *
			 */

			const string layout_results_format = "{{\"part\":{0},\"sheet\":{1},\"part_width\":{2},\"part_height\":{3},\"X\":{4},\"Y\":{5},"+
													"\"rotated\":\"{6}\", \"ilosc_resztek_z_arkusza\":\"{7}\"}}";			
			string callback = "";

			foreach (var dictionary in data)
			{
				callback += string.Format(layout_results_format,
					dictionary["part"],
					dictionary["sheet"],
					dictionary["part_width"],
					dictionary["part_height"],
					dictionary["X"],
					dictionary["Y"],
					dictionary["rotated"],
					dictionary["ilosc_resztek_z_arkusza"]
				);
				callback += ",";
			}

			callback = callback.Remove(callback.Length - 1);  // usuwa ostatni znak - ,
			return callback;
		}
			
		private static String __Material_arkusz_wiele_rozmiarow_results(Material material)
		{
			/*
			 * funkcja zmieniajaca dane na string
			 * dziala tylko na Materiale, ktory zostal dodany z Kalkulator.SheetMultipleSheetSize
			 *
			 * dane wysjsciowe - {"sheet": ... ,"wysokosc": ... ,"szerokosc": ... ,"ilosc": ... ,"nr_zamowienia": ... ,"kod": ... } , ...
			 *
			 *Material.__Arkusz_Stock_Items:
			 *  public int sheet_nr;  // nr arkusza, nr jest dodawany co 1 w zaleznosci od kolejnosci dodawania do kalkulatora.
				// sheet_nr potrzebne do scalenia informacji z kalkulatora i danymi Arkusz_Stock
			   	public double wysokosc;  //wyzsza wartosc arkusza (mm)
			   	public double szerokosc;  // nizsza wartosc (mm)
				public int ilosc;
				public string nr_zamowienia;  // opis (aID - text)
				public string kod;  // Towar.kod, mozn
			*/

			const string material_arkusz_results_format = "{{\"sheet\":{0},\"wysokosc\":{1},\"szerokosc\":{2},\"ilosc\":{3},"+
				"\"nr_zamowienia\":\"{4}\",\"kod\":\"{5}\",\"id_towar\":\"{6}\",\"id_odpad_arkusz\":\"{7}\",\"nr\":\"{8}\"}}";

			string callback = "";


			foreach (var arkusz_items in material.arkusz_lista())
			{
				callback += string.Format(material_arkusz_results_format,
					arkusz_items.sheet_nr.ToString(),
					arkusz_items.wysokosc.ToString(),
					arkusz_items.szerokosc.ToString(),
					arkusz_items.ilosc.ToString(),
					arkusz_items.nr_zamowienia.ToString(),
					arkusz_items.kod.ToString(),
					arkusz_items.id_towar.ToString(),
					arkusz_items.id_odpad_arkusz.ToString(),
					String.Format("{0}-{1}-{2}", arkusz_items.nr_zamowienia.ToString(), arkusz_items.sheet_nr.ToString(), arkusz_items.kod.ToString())
				);
				callback += ",";
			}

			callback = callback.Remove(callback.Length - 1);  // usuwa ostatni znak - ,

			foreach (var arkusz_items in material.arkusz_lista()) {
				Console.WriteLine ("Arkusz - wysokosc {0}, szerokosc {1}, ilosc {2}, nr namowienia {3}, kod {4} ",
					arkusz_items.wysokosc, arkusz_items.szerokosc, arkusz_items.ilosc, arkusz_items.nr_zamowienia, arkusz_items.sheet_nr);
			}


			return callback;
		}

		private static String __Material_formatka_results(Material material)
		{
			/*
			 * funkcja zmieniajaca dane na string
			 *
			 * dane wyjsciowe - {"sheet": ... ,"part": ... ,"wysokosc": ... ,"szerokosc": ... ,"nr_formatki": ... ,"nr_zamowienia": ... , "zachowac_strukture": ..., "ilosc": ... } , ...
			 *
			 *
			 *Material.__Formatka_Part_Items:
			 *	public int sheet_nr;  // nr arkusza, powiazany z kalkulatorem i arkuszem
			    public int part_nr; // nr formatki, nr jest dodawany co 1 w zaleznosci od kolejnosci dodawania do kalkulatora.
				// part_nr potrzebne do scalenia informacji z kalkulatora i danymi Formatka_Part
				public double wysokosc;
				public double szerokosc;
				public string nr_formatki;
				public string nr_zamowienia;
				public bool zachowac_strukture;  // dla kalkulatora oznaczamy czy moze obracac przy obliczeniach
				// jesli zachowac_strukture == True to oblicza bez obracania

				public int ilosc()
				{
					// dodajemy zawsze 1 poniewaz musimy miec unikalne opisy dla kazdej z formatek
					return 1;
				}

				public string opis_wymiar()
				{
					return this.wysokosc + "x" + this.szerokosc;
				}
			*/

			const string material_formatka_results_format = "{{\"sheet\":{0},\"part\":{1},\"wysokosc\":{2},\"szerokosc\":{3},"+
															"\"nr_formatki\":{4},\"nr_zamowienia\":{5},\"zachowac_strukture\":{6},\"ilosc\":{7}}}";

			string callback = "";

			foreach (var formatka_items in material.formatka_lista())
			{
				callback += String.Format(material_formatka_results_format,
					formatka_items.sheet_nr,
					formatka_items.part_nr,
					formatka_items.wysokosc,
					formatka_items.szerokosc,
					formatka_items.nr_formatki,
					formatka_items.nr_zamowienia,
					formatka_items.zachowac_strukture,
					formatka_items.ilosc()
				);
				callback += ",";
			}
			return callback;
		}

		public static void Agreguj_material(Material material)
		{
			Console.WriteLine ("Agregator_Danych.cs============================");
			Console.WriteLine ("Zagregowane dane z Material (arkusze, formatki) : ");
			string dane_z_agregatora_dla_arkuszy = Agregator_Danych.__Material_arkusz_wiele_rozmiarow_results (material);
			string dane_z_agregatora_dla_formatek = Agregator_Danych.__Material_formatka_results (material);
			Console.WriteLine ("\nArkusze = {0} ", dane_z_agregatora_dla_arkuszy);
			Console.WriteLine ("\nFormatki = {0}", dane_z_agregatora_dla_formatek);
			Console.WriteLine ("\nEND====================================\n");
		}

		public static String pokaz_kod_arkusza(Material material)
		{
			string kod = "";
			foreach (var arkusz_items in material.arkusz_lista())
			{
				kod = arkusz_items.kod.ToString ();
			}
			return kod;
		}

		public static List<int> wyswietl_arkusze_sheet_do_uzycia(List<Dictionary<string, string>> cut_results){
			/*
			 * zwraca informacje o tym ktory arkusz bedzie wykorzystany przy cieciu
			 * potrzebuje takiej informacji przy generowaniu zdjec itp.
			*/
			List<int> uzyte_arkusz_sheet = new List<int>();

			foreach (var dictionary in cut_results)
			{
				bool exists = uzyte_arkusz_sheet.Exists (element => element == Convert.ToInt32(dictionary ["sheet"]));
				if (exists == false) {  
					uzyte_arkusz_sheet.Add (Convert.ToInt32(dictionary ["sheet"]));
				}
			}
			return uzyte_arkusz_sheet;
		}

		public static Dictionary<string, string> wyswietl_i_przygotuj_dane_do_przekazania(
			List<Dictionary<string, string>> layout_results, 
			List<Dictionary<string, string>> cut_results,
			Material material
		)
		{
			/*
			 * wywietla dane dla arkuszy o wielu rozmiarach, dodawane pojedynczo 
			 * 
			 * z CutGlib (kalkulatora):
			 *
			 * cut_results = "{"sheet": ... , "sheet_width": ... , "sheet_height": ... , cuts_count: ... } , ... "
			 *
			 * cut_points =  { "sheet": ... , "cut": ... , "X1": ... , "Y1": ... , "X2": ... , "Y2": ...  } , ...
			 *
			 * all_cuts = Part Count
			 *
			 * cut_results - lista arkuszy z wymiarami i ilosciami ciec
			 * cut_points - punkty cięcia
			 *
			 * sheet - nr arkusza (0-...), nr jest dodawany co 1 w zaleznosci od kolejnosci dodawania do kalkulatora
			 * cuts_count = ilosc ciec na arkuszu
			 * all_cuts = ilosc ciec wszystkich (wszystkie arkusze)
			 *
			 * layout_results = { "part" : ... , "sheet" : ... , "part_width" : ..., part_height : ... , X: ... , Y: ... , rotated: ... , ilosc_resztek_z_arkusza : ... }
			 *
			 * part - nr formatki (0-...), nr jest dodawany co 1 w zaleznosci od kolejnosci dodawania do kalkulatora
			 * rotated - czy mozna obracac formatke przy obliczeniu, jesli ma zachowac strukture to ten parametr musi byc false
			 *
			*/
			Console.WriteLine ("Agregator_Danych.cs============================");
			Console.WriteLine ("zwrotka dla programu zewnetrznego : ");

			string layout_results_as_string = Agregator_Danych.__CUTGLIB_layout_results (layout_results, 0);
			string cut_results_as_string = Agregator_Danych.__CUTGLIB_cut_results (cut_results);
			string sheet_results = Agregator_Danych.__Material_arkusz_wiele_rozmiarow_results (material);

			Console.WriteLine ("\nlayout = {0}", layout_results_as_string);
			Console.WriteLine ("\ncuts = {0}", cut_results_as_string);
			Console.WriteLine ("\nsheet = {0}", sheet_results);
			Console.WriteLine ("\nEND====================================\n");

			Dictionary<string, string> callback = new Dictionary<string, string> ();
			callback.Add ("layout", layout_results_as_string);
			callback.Add ("cut", cut_results_as_string);
			callback.Add ("sheet", sheet_results);

			return callback;
		}

	}
}
