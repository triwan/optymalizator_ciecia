import Pyro4
import Pyro4.util
import inspect
import sys
import os.path

#add-ons
sys.path.append(os.path.abspath('../'))
from logger import Logger

def return_pyro_except_on_failure():
  def decorate(f):
    def applicator(*args, **kwargs):
      try:
         return f(*args,**kwargs)
      except Exception:
         logger = Logger()
         logger.error(inspect.stack(), "Pyro traceback:")
         logger.error(inspect.stack(), ('\n',"".join(Pyro4.util.getPyroTraceback())))
         #print("Pyro traceback:")
         #print("".join(Pyro4.util.getPyroTraceback()))
         
         opis = 'Błąd na serwerze PYRO'
         #probuje pobrac sam EXCEPTION do wyświetlenia w opisie
         try:
             cala_fraza=  "".join(Pyro4.util.getPyroTraceback())
             cala_fraza_split = cala_fraza.split('ValueError\'>:')  # dziele
             opis = cala_fraza_split[2].split('| -')[0].strip()  #dziele i usuwam pusty znak z przodu
         except:
          pass
         
    return applicator
  return decorate
