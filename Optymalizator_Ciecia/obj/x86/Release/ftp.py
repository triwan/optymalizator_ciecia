# -*- coding: utf-8 -*-

"""
Module implementing Ftp.
"""
import os 

import client.config as config
import ftplib

from forms.add_ons.progress_bar import Progress_Bar


class FtpUploadTracker:
    sizeWritten = 0
    totalSize = 0
    lastShownPercent = 0

    def __init__(self, totalSize,  progress_bar):
        self.totalSize = totalSize
        self.progress_bar = progress_bar
        
    def handle(self, block,  block_size=0):
        self.sizeWritten += 1024
        self.sizeWritten += block_size
        
        percentComplete = round((self.sizeWritten / self.totalSize) * 100)
        
        if (self.lastShownPercent != percentComplete):
            self.lastShownPercent = percentComplete
            if self.progress_bar is not False:
                self.progress_bar.change_progress(percentComplete)

class Ftp:
    """
    uzycie:
    from forms.ftp.ftp import Ftp
    ftp = Ftp()
    ftp.upload(remote_path='/files/dokumenty_klient/',  file_path_to_upload='/home/mateusz/Wideo/1.mp4', progress_bar=True)
    ftp.download(remote_path='/files/dokumenty_klient/',  remote_file_name='a',  download_file_path='/home/mateusz/Temp/cccc', progress_bar=True)            
    """            
    def __init__(self, parent=None):
        """
        Constructor
        
        @param parent reference to the parent widget (QWidget)
        """
        self.server = '172.17.4.117' #config.ftp['serwer']  #'172.17.0.117'
        self.username = config.ftp['uzytkownik']
        self.password = config.ftp['haslo']         
        
        self.__connect()
    
    def __connect(self):                
        try:
            self.ftp_connection = ftplib.FTP(self.server, self.username, self.password,  timeout=3)        
        except Exception as e:
            import forms.add_ons.message_box  as msg
            msg.message_box('Błąd FTP',  str(e)[0:600],  str(e),  critical=True)
        
    def __handle_download(self,  block):
        '''
        dla progress bar
        '''
        self.upload_tracker.handle(block=block,  block_size=len(block))
        self.file.write(block)

    def show_progress_bar(self,  show):    
        if show:
            self.ui = Progress_Bar()
            self.ui.show()                                
        else:
            self.ui = False
    
    def upload(self,  remote_path,  file_path_to_upload,  progress_bar=True):
        self.ftp_connection.cwd(remote_path)                
        self.show_progress_bar(progress_bar)
        
        fh = open(file_path_to_upload, 'rb')
        
        totalSize = os.path.getsize(file_path_to_upload)
        self.upload_tracker = FtpUploadTracker(int(totalSize),  self.ui)
                
        self.ftp_connection.storbinary('STOR a', fh,  1024,  self.upload_tracker.handle)
        
        fh.close()        
        
        return True

    def download(self,  remote_path,  remote_file_name,  download_file_path,  progress_bar=True): 
        self.ftp_connection.sendcmd("TYPE i")    # Switch to Binary mode        
        self.show_progress_bar(progress_bar)        
        
        remote_file_path = ('%s%s') % (remote_path,  remote_file_name)
        totalSize = self.ftp_connection.size(remote_file_path)
                
        self.upload_tracker = FtpUploadTracker(int(totalSize),  self.ui)
                
        self.file = open(download_file_path, 'wb')        
        self.ftp_connection.retrbinary('RETR ' + remote_file_path,  self.__handle_download)
        
        self.file.close()                
        
        return True
