from __future__ import print_function
import sys
import os.path
import inspect
import Pyro4

# external
sys.path.append(os.path.abspath('../'))

from pyro_utils import return_pyro_except_on_failure

pyro_name = 'optymalizator'
pyro_server_ip = '127.0.0.1'
pyro_port = '5000'
uri = dict(
        Opytmalizacja_Proxy = ('PYRO:%s.OPTYMALIZATOR@%s:%s' % (pyro_name, pyro_server_ip, pyro_port)),
        Agregator_Argumentow_Proxy = ('PYRO:%s.AGREGATOR_ARGUMENTOW@%s:%s' % (pyro_name, pyro_server_ip, pyro_port))
    )


class Optymalizator_Proxy(object):
    def __init__(self):
        self.uri = uri['Opytmalizacja_Proxy']
        self.OBJECT = Pyro4.Proxy(self.uri)

    @return_pyro_except_on_failure()
    def dodaj_arkusz(self, wysokosc:int, szerokosc:int, kod:str):
        self.OBJECT.dodaj_arkusz(wysokosc, szerokosc, kod)

    @return_pyro_except_on_failure()
    def dodaj_nr_zamowienia(self, nr_zamowienia:str):
        self.OBJECT.dodaj_nr_zamowienia(nr_zamowienia)

    @return_pyro_except_on_failure()
    def dodaj_formatke(self, wysokosc:int, szerokosc:int, nr_formatki:str, zachowac_strukture:bool):
        self.OBJECT.dodaj_formatke(wysokosc, szerokosc, nr_formatki, zachowac_strukture)

    @return_pyro_except_on_failure()
    def obetnij_boki_arkusza_trim(self, lewy_bok:int, gorny_bok:int, prawy_bok:int, dolny_bok:int):
        self.OBJECT.obetnij_boki_arkusza_trim(lewy_bok, gorny_bok, prawy_bok, dolny_bok)

    @return_pyro_except_on_failure()
    def definiuj_grubosc_pily(self, grubosc:int):
        self.OBJECT.definiuj_grubosc_pily(grubosc)

    @return_pyro_except_on_failure()
    def minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia(self, minimalizuj:bool):
        self.OBJECT.minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia(minimalizuj)

    @return_pyro_except_on_failure()
    def definiuj_ilosc_ciec_na_pasie(self, ilosc:int):
        self.OBJECT.definiuj_ilosc_ciec_na_pasie(ilosc)





a = Optymalizator_Proxy()
a.dodaj_nr_zamowienia("76543456467")

# wartosci w mm
a.dodaj_arkusz(2000,1500,"9999")
a.dodaj_arkusz(2400,1500,"19999")
a.dodaj_arkusz(2300,1500,"29999")
a.dodaj_arkusz(2300,1500,"29999")
a.dodaj_arkusz(2300,1500,"29999")

# wartosci w mm
a.dodaj_formatke(900,800,"qqq",False)
a.dodaj_formatke(1900,800,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)

a.obetnij_boki_arkusza_trim(20,20,20,20)  # wartosci w mm
a.definiuj_grubosc_pily(30)  # wartosci w mm
a.minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia(True)
a.definiuj_ilosc_ciec_na_pasie(4)

a.OBJECT.generuj_dane()

print(a.OBJECT.pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy())
print(a.OBJECT.pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy())
print(a.OBJECT.pokaz_ilosc_ciec_bez_trim_dla_wszystkich_arkuszy())
print(a.OBJECT.pokaz_arkusze_ktore_nie_zostana_pociete())
print(a.OBJECT.pokaz_sheet_nr_arkuszy_ktore_zostana_pociete())
print(a.OBJECT.pokaz_ilosc_arkuszy_do_pociecia())
print(a.OBJECT.pokaz_arkusze_ktore_zostana_pociete())
print(a.OBJECT.pokaz_arkusze_ktore_nie_zostana_pociete())
print(a.OBJECT.pokaz_ciecia_dla_konkretnego_arkusza(1))  # sheet
print(a.OBJECT.pokaz_arkusz(1))  # sheet
print(a.OBJECT.pokaz_obrazy_dla_arkusza(1))  # sheet
print(a.OBJECT.pokaz_formatki_arkusza(1))  # sheet
