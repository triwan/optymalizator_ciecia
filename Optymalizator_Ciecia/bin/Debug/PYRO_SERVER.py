import Pyro4
import sys
import os.path
import inspect

import logging

sys.path.append(os.path.abspath('../'))
from Python_Czytacz_Optymalizator_Ciecia import Optymalizator

if sys.version_info < (3, 0):
    raise "musisz uzyc python-a w wersji 3.x"

pyro = dict(
    name = 'optymalizator',
    host = '127.0.0.1',
    port = 5000
)

def main():
    #netstat -antu   - sprawdz wolne porty
    Pyro4.config.DETAILED_TRACEBACK=1  # detale wyjatkow
    Pyro4.config.HOST = pyro['host']
    Pyro4.config.THREADPOOL_SIZE = 300
    Pyro4.Daemon.serveSimple(
            {
                Optymalizator: ("%s.OPTYMALIZATOR" % pyro['name']),
            },
            ns=False, port=pyro['port']
    )

if __name__=="__main__":
    main()
