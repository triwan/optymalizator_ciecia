"""
Python_Czytacz_Optymalizator_Ciecia uruchamiamy wersja python 2.x .
Program wywoluje, za pomoca wine, program Optymalizator_Ciecia.exe.
Uruchamiajac prgram Optymalizator_Ciecia.exe wskazuje port na ktory ma zostac wyslana informacja zwrotna
Python_Czytacz_Optymalizator_Ciecia nasluchuje na wskazanym porcie do momentu przekazania informacji

przykladowe uzycie:

a = Optymalizator()
a.dodaj_nr_zamowienia("76543456467")

# wartosci w mm
a.dodaj_arkusz(2000,1500,"9999")
a.dodaj_arkusz(2400,1500,"19999")
a.dodaj_arkusz(2300,1500,"29999")
a.dodaj_arkusz(2300,1500,"29999")
a.dodaj_arkusz(2300,1500,"29999")

# wartosci w mm
a.dodaj_formatke(900,800,"qqq",False)
a.dodaj_formatke(1900,800,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)

a.obetnij_boki_arkusza_trim(20,20,20,20)  # wartosci w mm
a.definiuj_grubosc_pily(30)  # wartosci w mm
a.minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia(True)
a.definiuj_ilosc_ciec_na_pasie(4)

a.generuj_dane()

print(a.Agregator_Argumentow.pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy())
print(a.Agregator_Argumentow.pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy())
print(a.Agregator_Argumentow.pokaz_ilosc_ciec_bez_trim_dla_wszystkich_arkuszy())
print(a.Agregator_Argumentow.pokaz_arkusze_ktore_nie_zostana_pociete())
print(a.Agregator_Argumentow.pokaz_sheet_nr_arkuszy_ktore_zostana_pociete())
print(a.Agregator_Argumentow.pokaz_ilosc_arkuszy_do_pociecia())
print(a.Agregator_Argumentow.pokaz_arkusze_ktore_zostana_pociete())
print(a.Agregator_Argumentow.pokaz_arkusze_ktore_nie_zostana_pociete())
print(a.Agregator_Argumentow.pokaz_ciecia_dla_konkretnego_arkusza(1))  # sheet
print(a.Agregator_Argumentow.pokaz_arkusz(1))  # sheet
print(a.Agregator_Argumentow.pokaz_obrazy_dla_arkusza(1))  # sheet
print(a.Agregator_Argumentow.pokaz_formatki_arkusza(1))  # sheet

"""
import sys
import ast
import socket

import sys

import Pyro4

#if sys.version_info > (3, 0):
#    raise "musisz uzyc python-a w wersji 2.x. W przeciwnym wypadku socket nie bedzie dobrze dzialac. Problem wystepuje w momencie sprawdzania czy port jest uzywany czy nie \
#			jesli uda sie na python3 zmienic funkcje Socket_Server.check_socket"

@Pyro4.expose
class Agregator_Argumentow:
    def __init__(self, json_string):
        self.dane = self.konwertuj_dane_wejsciowe(json_string)  # {'':[ {} , ... ] , ... }

    def konwertuj_dane_wejsciowe(self, json_string):
        """
        string na:

            {
                u'sheet':
                    [
                        {u'ilosc': 1, u'sheet': 0, u'kod': u'9999 ', u'wysokosc': 2000, u'nr_zamowienia': u'76543456467', u'szerokosc': 1500},
                        {u'ilosc': 1, u'sheet': 1, u'kod': u'19999 ', u'wysokosc': 2400, u'nr_zamowienia': u'76543456467', u'szerokosc': 1500},
                        {u'ilosc': 1, u'sheet': 2, u'kod': u'29999 ', u'wysokosc': 2300, u'nr_zamowienia': u'76543456467', u'szerokosc': 1500},
                        {u'ilosc': 1, u'sheet': 3, u'kod': u'29999 ', u'wysokosc': 2300, u'nr_zamowienia': u'76543456467', u'szerokosc': 1500},
                        {u'ilosc': 1, u'sheet': 4, u'kod': u'29999 ', u'wysokosc': 2300, u'nr_zamowienia': u'76543456467', u'szerokosc': 1500}
                    ],
                u'cut':
                    [
                        {u'trim': u'True', u'Y2': 5, u'sheet': 0, u'X2': 1500, u'cut': 0, u'Y1': 5, u'X1': 0},
                        {u'trim': u'True', u'Y2': 1995, u'sheet': 0, u'X2': 1500, u'cut': 1, u'Y1': 1995, u'X1': 0},
                        {u'trim': u'True', u'Y2': 1980, u'sheet': 0, u'X2': 5, u'cut': 2, u'Y1': 20, u'X1': 5},
                        {u'trim': u'True', u'Y2': 1980, u'sheet': 0, u'X2': 1495, u'cut': 3, u'Y1': 20, u'X1': 1495},
                        {u'trim': u'False', u'Y2': 1995, u'sheet': 0, u'X2': 835, u'cut': 0, u'Y1': 5, u'X1': 835},
                        {u'trim': u'False', u'Y2': 1935, u'sheet': 0, u'X2': 835, u'cut': 1, u'Y1': 1935, u'X1': 5},
                        {u'trim': u'True', u'Y2': 5, u'sheet': 1, u'X2': 1500, u'cut': 0, u'Y1': 5, u'X1': 0},
                        {u'trim': u'True', u'Y2': 2295, u'sheet': 1, u'X2': 1500, u'cut': 1, u'Y1': 2295, u'X1': 0},
                        {u'trim': u'True', u'Y2': 2280, u'sheet': 1, u'X2': 5, u'cut': 2, u'Y1': 20, u'X1': 5},
                        {u'trim': u'True', u'Y2': 2280, u'sheet': 1, u'X2': 1495, u'cut': 3, u'Y1': 20, u'X1': 1495},
                        {u'trim': u'False', u'Y2': 935, u'sheet': 1, u'X2': 1495, u'cut': 0, u'Y1': 935, u'X1': 5},
                        {u'trim': u'False', u'Y2': 1765, u'sheet': 1, u'X2': 1495, u'cut': 1, u'Y1': 1765, u'X1': 5},
                        {u'trim': u'False', u'Y2': 935, u'sheet': 1, u'X2': 735, u'cut': 2, u'Y1': 5, u'X1': 735},
                        {u'trim': u'False', u'Y2': 935, u'sheet': 1, u'X2': 1465, u'cut': 3, u'Y1': 5, u'X1': 1465},
                        {u'trim': u'False', u'Y2': 1765, u'sheet': 1, u'X2': 935, u'cut': 4, u'Y1': 935, u'X1': 935},
                        {u'trim': u'True', u'Y2': 5, u'sheet': 2, u'X2': 1500, u'cut': 0, u'Y1': 5, u'X1': 0},
                        {u'trim': u'True', u'Y2': 2295, u'sheet': 2, u'X2': 1500, u'cut': 1, u'Y1': 2295, u'X1': 0},
                        {u'trim': u'True', u'Y2': 2280, u'sheet': 2, u'X2': 5, u'cut': 2, u'Y1': 20, u'X1': 5},
                        {u'trim': u'True', u'Y2': 2280, u'sheet': 2, u'X2': 1495, u'cut': 3, u'Y1': 20, u'X1': 1495},
                        {u'trim': u'False', u'Y2': 935, u'sheet': 2, u'X2': 1495, u'cut': 0, u'Y1': 935, u'X1': 5},
                        {u'trim': u'False', u'Y2': 935, u'sheet': 2, u'X2': 735, u'cut': 1, u'Y1': 5, u'X1': 735cieciuie}
                    ],
                u'layout':
                    [
                        {u'sheet': 1, u'rotated': u'False', u'part': 3, u'part_width': 700, u'Y': 20, u'X': 20, u'part_height': 900},
                        {u'sheet': 1, u'rotated': u'False', u'part': 2, u'part_width': 700, u'Y': 20, u'X': 750, u'part_height': 900},
                        {u'sheet': 1, u'rotated': u'True', u'part': 0, u'part_width': 800, u'Y': 950, u'X': 20, u'part_height': 900},
                        {u'sheet': 2, u'rotated': u'False', u'part': 4, u'part_width': 700, u'Y': 20, u'X': 20, u'part_height': 900},
                        {u'sheet': 0, u'rotated': u'False', u'part': 1, u'part_width': 800, u'Y': 20, u'X': 20, u'part_height': 1900}
                    ],
                u'obrazy':
                    [
                        {u'plik': u'76543456467_0.png', u'sheet': 0},
                        {u'plik': u'76543456467_1.png', u'sheet': 1},
                        {u'plik': u'76543456467_2.png', u'sheet': 2}
                    ]
            }


        sheet = nr arkusza, nr jest przypisywany w momencie dodawania argumentu do kalkulatora
        part = nr formatki, dodawane tj sheet
        kod = Towar.kod lub kod_odpadu
        """
        try:
            import json
            parsed_json = None
            parsed_json = json.loads(json_string)
            return parsed_json
        except Exception as e:
            raise ValueError('Nieprawdiłowy format danych z kalkulatora przy przetwarzniu obiektu string na obiekt json.\n----------\nObiekt string = %s\n----------\nError = %s' % (json_string, e))

    def ilosc_ciec(self):
        ciecia_bez_trim = [cut for cut in self.dane['cut'] if cut['trim']!='True']  # nie wliczajac trim
        return len(ciecia_bez_trim)

    def pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy(self):
        # zwraca wszystkie informacje o ciecia dla wszystkich wykorzystanych arkuszy
        ciecia_bez_trim_dla_calego_zamowienia = [cut for cut in self.dane['cut'] if cut['trim']!='True']  # nie wliczajac trim, wszystkie ciecia
        ciecia_bez_trim_dla_calego_zamowienia = sorted(ciecia_bez_trim_dla_calego_zamowienia, key=lambda k: k['sheet'])  # sortuje po sheet
        return ciecia_bez_trim_dla_calego_zamowienia  # np.: [{u'trim': u'False', u'cut': 0, u'sheet': 0, u'X1': 835, u'Y1': 5, u'X2': 835, u'Y2': 1995}, ... ]

    def pokaz_ilosc_ciec_bez_trim_dla_wszystkich_arkuszy(self):
        # zwraca ilosc ciec jaka zostanie wykonana dla wszystkich formatek, arkuszy
        ilosc_wszystkich_ciec_dla_zamowienia = len([cut for cut in self.dane['cut'] if cut['trim']!='True'])  # nmowienia': u'76543456467', u'szerokosc': 1500}, ... ]
        return ilosc_wszystkich_ciec_dla_zamowienia

    def pokaz_ilosc_ciec_bez_trim_dla_konkretnego_arkusza(self, sheet_nr):
        # zwraca ilosc ciec jaka zostanie wykonana dla wszystkich formatek, arkuszy
        ilosc_ciec_dla_arkusza = len([cut for cut in self.dane['cut'] if cut['sheet']==sheet_nr and cut['trim']!='True'])
        return ilosc_ciec_dla_arkusza

    def pokaz_arkusze_ktore_nie_zostana_pociete(self):
        # zwraca liste z arkuszami ktore nie beda wykorzystane przy cieciuie wliczajac trim, wszystkie ciecia
        return ilosc_wszystkich_ciec_dla_zamowienia  # zwraca np.: 23

    def pokaz_sheet_nr_arkuszy_ktore_zostana_pociete(self):
        # zwraca sheet (sheet_nr) arkuszy, ktore zostana wykorzystane do ciecia formatek
        arkusze_do_ciecia_same_nr_sheet = list(set([cut['sheet'] for cut in self.dane['cut'] if cut['trim']!='True']))  # same nr sheet, [ , ] unikalne
        return arkusze_do_ciecia_same_nr_sheet # np.: [0, 1, 2]

    def pokaz_ilosc_arkuszy_do_pociecia(self):
        # zwraca sama ilosc
        ilosc_arkuszy_do_pociecia = len(list(set([cut['sheet'] for cut in self.dane['cut'] if cut['trim']!='True'])))
        return ilosc_arkuszy_do_pociecia # np.: 3

    def pokaz_arkusze_ktore_zostana_pociete(self):
        # zwraca liste z arkuszami do pociecia
        lista_arkuszy_do_pociecia = [cut for cut in self.dane['sheet'] if cut['sheet'] in self.pokaz_sheet_nr_arkuszy_ktore_zostana_pociete()]
        return lista_arkuszy_do_pociecia  # [{u'ilosc': 1, u'sheet': 0, u'kod': u'9999 ', u'wysokosc': 2000, u'nr_zamowienia': u'76543456467', u'szerokosc': 1500}, ... ]

    def pokaz_arkusze_ktore_nie_zostana_pociete(self):
        # zwraca liste z arkuszami ktore nie beda wykorzystane przy cieciu
        lista_arkuszy_nie_do_pociecia = [cut for cut in self.dane['sheet'] if cut['sheet'] not in self.pokaz_sheet_nr_arkuszy_ktore_zostana_pociete()]
        return lista_arkuszy_nie_do_pociecia  # [{u'ilosc': 1, u'sheet': 0, u'kod': u'9999 ', u'wysokosc': 2000, u'nr_zamowienia': u'76543456467', u'szerokosc': 1500}, ... ]

    def pokaz_ciecia_dla_konkretnego_arkusza_bez_trim(self, sheet_nr):
        # zwraca ciecia, ktore beda wykonane na konkretnej formatce
        ciecia_arkusza = [cut for cut in self.dane['cut'] if cut['sheet']==sheet_nr and cut['trim']!='True']
        return ciecia_arkusza  # [{u'trim': u'True', u'cut': 0, u'sheet': 0, u'X1': 0, u'Y1': 5, u'X2': 1500, u'Y2': 5}, ... ]

    def pokaz_ciecia_dla_konkretnego_arkusza(self, sheet_nr):
        # zwraca ciecia, ktore beda wykonane na konkretnej formatce
        ciecia_arkusza = [cut for cut in self.dane['cut'] if cut['sheet']==sheet_nr]
        return ciecia_arkusza  # [{u'trim': u'True', u'cut': 0, u'sheet': 0, u'X1': 0, u'Y1': 5, u'X2': 1500, u'Y2': 5}, ... ]

    def pokaz_arkusz(self, sheet_nr):
        # zwraca dane o konkretnym arkuszu
        arkusz = [sheet for sheet in self.dane['sheet'] if sheet['sheet']==sheet_nr]
        return arkusz  # np.: [{u'ilosc': 1, u'sheet': 1, u'kod': u'19999 ', u'wysokosc': 2400, u'nr_zamowienia': u'76543456467', u'szerokosc': 1500}]

    def pokaz_obrazy_dla_arkusza(self, sheet_nr):
        # wyswietla obraz dla konkretnego arkusza
        # wzór nazwy = nr_zamowienia-sheet_nr-dekor.png
        obraz_z_cieciami = [pic for pic in self.dane['obrazy'] if pic['sheet']==sheet_nr]
        return obraz_z_cieciami  # [{u'plik': u'76543456467_0.png', u'sheet': 0}]

    def pokaz_formatki_arkusza(self, sheet_nr):
        # wyswietla formaki dla konkretnego arkusza
        formatki = [sheet for sheet in self.dane['layout'] if sheet['sheet']==sheet_nr]
        formatki = sorted(formatki, key=lambda k: k['part'])  # sortuje po part
        return formatki  # [{u'sheet': 1, u'rotated': u'True', u'part': 0, u'part_width': 800, u'Y': 950, u'X': 20, u'part_height': 900}, ... ]

    def pokaz_resztki_arkusza(self, sheet_nr):
        # wyswietla formaki dla konkretnego arkusza
        formatki = [sheet['ilosc_resztek_z_arkusza'] for sheet in self.dane['layout'] if sheet['sheet']==sheet_nr]
        return formatki[0]  # dla formatek z danego arkusza zawsze bedzie taka sama liczba ilosc_resztek_z_arkusza np. (10,10,10 ...)


class Socket_Server(object):
	"""
	przed uruchomieniem Optymalizator_Ciecia.exe ustawiam socket do nasluchiwania na losowym porcie self.PORT = random.randint(1025,65535)
	Wysylam informacje do programu Optymalizator_Ciecia.exe za pomoca --param99 o porcie gdzie ma wyslac z programu zwrotke
	Po dostaniu zwrotki wylaczam Socket_Server
	"""
	def __init__(self):
		self.HOST = ''   # Symbolic name, meaning all available interfaces
		self.PORT = None  # Arbitrary non-privileged portpokaz_resztki_arkusza

		self.keep_talking = True  # jesli tru to talk, false tzn ze mam skonczyc talking, potrzebne do thread

		self.check_socket()

		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print('Socket created')

		#Bind socket to local host and port
		try:
		    self.sock.bind((self.HOST, self.PORT))
		except socket.error as msg:
		    print('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
		    sys.exit()

		print('Socket bind complete')

		#Start listening on socket
		self.sock.listen(1)  # w kolejce max 1
		print('Socket now listening')

	def __delete__(self):
		self.sock.shutdown(self.sock.SHUT_RDWR)
		self.sock.close()

	def talking(self):
		#now keep talking with the client
		self.data = None
		while self.keep_talking:
		    #wait to accept a connection - blocking call
		    conn, addr = self.sock.accept()
		    print('Connected with ' + addr[0] + ':' + str(addr[1]))
		    self.data = conn.recv(9999999).decode("ascii")

	def check_socket(self):
		import random
		from contextlib import closing

		self.PORT = random.randint(1025,65535)
		with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
			if sock.connect_ex((self.HOST, self.PORT)) == 0:
				print("Port %s jest zajety" % self.PORT)
				self.check_socket()
			else:
				print("Port %s nie jest uzywany" % self.PORT)

@Pyro4.expose
class Optymalizator(object):
    def __init__(self):
    	self.__cmd = "wine ./Optymalizator_Ciecia.exe"

    	# wszystkie parametry podajem w mm
    	self.__parametr_zamowienie = None # param2 ... jakis tam nr zamowienia
    	self.__parametr_arkusz = None  # param3 [(double) wysokosc, (double) szerokosc, (strin) kod]&....
    	self.__parametr_formatka = None  # param4 [(double) wysokosc, (double) szerokosc,(string) nr_formatkim, (bool) zachowac_strukture]&...
    	# ustawienie optymalizatora
    	self.__parametr_trim = None  # param5 [20,30,40,50] [lewy bok, gorny bok, prawy bok, dolny bok]  odcinanie bokow arkusza
    	self.__grubos_pily = None  # param6 0...
    	self.__minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia = True  #param7 true/false - przy optymalizacji nie kalkuluje ciecia tak aby minimalizowac ilosci ciec na resztkach
    	self.__ilosc_ciec_na_pasie = 6  # param8 od 2 do 6 - czym mniejsza wartosc tym mniej ciec na pocietym pasie ale mniej optymalne ciecia
    	# ustawienia dla socket
    	self.__socket_port = 0  # param99 wysylamy do programu Optymalizator_Ciecia.exe nr portu na ktorym ma zwrocic dane
    	self.__horizontal_cut_direction = True  # Suitable for roll (stripe) cutting. If aRoolMode is True then it ensures the first cut is made completely from the stock’s left side to the right one.
    	self.__vertical_cut_direction = None  # Suitable for roll (stripe) cutting. If aRoolMode is True then it ensures the first cut is made completely from the stock’s top to the bottom.
    	self.__auto_cut_direction = None  # Defines the calculation engine will automatically detect what direction produce better results and use this direction
    	self.__max_layout_size = 1  # Definiuje ilosc plyt, ktore moga byc ciete naraz na pilarce. Dziala tylko jesli UseLayoutMinimization == true
        # obraz
    	self.__sciezka_do_obrazu = None  # jesli nie podamy sciezki program nie bedzie w ogole generowal pliku


    	self.s = Socket_Server()

    def ustaw_vertical_cut_direction(self):
    	self.__horizontal_cut_direction = False
    	self.__auto_cut_direction = False

    	self.__vertical_cut_direction = True

    def ustaw_horizontal_cut_direction(self):
    	self.__auto_cut_direction = False
    	self.__vertical_cut_direction = False

    	self.__horizontal_cut_direction = True

    def ustaw_auto_cut_direction(self):
    	self.__vertical_cut_direction = False
    	self.__horizontal_cut_direction = False

    	self.__auto_cut_direction = True

    def ustaw__max_layout_size(self, ilosc):
    	self.__max_layout_size = ilosc

    def generuj_obraz_podaj_sciezke(self, sciezka):
    	self.__sciezka_do_obrazu = sciezka

    def dodaj_nr_zamowienia(self, nr_zamowienia):
    	self.__parametr_zamowienie = nr_zamowienia

    def zeruj_parametry_arkuszy_i_formatek(self):
    	# wszystkie parametry sa zerowane (arkusz, formatka)
    	self.__parametr_arkusz = None  # param3 [(double) wysokosc, (double) szerokosc, (strin) kod] & ....
    	self.__parametr_formatka = None  # param4 [(double) wysokosc, (double) szerokosc,(string) nr_formatkim, (bool) zachowac_strukture]&...
    	self.__parametr_zamowienie = None

    def zeruj_parametry_ustawien(self):
    	self.__parametr_trim = None
    	self.__grubos_pily = None
    	self.__minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia = True
    	self.__ilosc_ciec_na_pasie = None
    	self.__socket_port = 0  # param99 wysylamy do programu Optymalizator_Ciecia.exe nr portu na ktorym ma zwrocic dane


    def dodaj_arkusz(self, wysokosc, szerokosc, kod, id_towar, id_odpad_arkusz):
        temp_arkusz = "[%s,%s,%s,%s,%s]" % (wysokosc, szerokosc, kod, id_towar, id_odpad_arkusz)

        if self.__parametr_arkusz is None:
            self.__parametr_arkusz = temp_arkusz
        else:
            self.__parametr_arkusz = ("%s&%s") % (self.__parametr_arkusz, temp_arkusz)

    def dodaj_formatke(self, wysokosc, szerokosc, nr_formatki, zachowac_strukture):
    	temp_formatka = "[%s,%s,%s,%s]" % (wysokosc, szerokosc, nr_formatki, zachowac_strukture)
    	if self.__parametr_formatka is None:
    		self.__parametr_formatka = temp_formatka
    	else:
    		self.__parametr_formatka = ("%s&%s") % (self.__parametr_formatka, temp_formatka)

    def obetnij_boki_arkusza_trim(self, lewy_bok, gorny_bok, prawy_bok, dolny_bok):
    	self.__parametr_trim = "[%s,%s,%s,%s]" % (lewy_bok, gorny_bok, prawy_bok, dolny_bok)
    	return

    # ustawienie optymalizatora
    def definiuj_grubosc_pily(self, grubosc):
    	if grubosc >=0:
    		self.__grubos_pily = grubosc

    def minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia(self, minimalizuj):
    	# domyslnie True
    	self.__minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia = minimalizuj

    def definiuj_ilosc_ciec_na_pasie(self, ilosc):
    	if ilosc >= 2 and ilosc <= 6:
    		self.__ilosc_ciec_na_pasie = ilosc

    # ustawienia dla socket
    def __port_do_komunikacji(self, port):
    	# wysylamy do programu Optymalizator_Ciecia.exe nr portu na ktorym ma zwrocic dane
    	self.__socket_port = port

    def __dodaj_parametry_do_zapytania(self):
    	self.__cmd = "wine ./Optymalizator_Ciecia.exe"
    	if self.__parametr_zamowienie is not None:
    		self.__cmd = "%s --param2=\"%s\"" % (self.__cmd, self.__parametr_zamowienie)
    	if self.__parametr_arkusz is not None:
    		self.__cmd = "%s --param3=\"%s\"" % (self.__cmd, self.__parametr_arkusz)
    	if self.__parametr_formatka is not None:
    		self.__cmd = "%s --param4=\"%s\"" % (self.__cmd, self.__parametr_formatka)
    	# ustawienie optymalizatora
    	if self.__parametr_trim is not None:
    		self.__cmd = "%s --param5=\"%s\"" % (self.__cmd, self.__parametr_trim)
    	if self.__grubos_pily is not None:
    		self.__cmd = "%s --param6=\"%s\"" % (self.__cmd, self.__grubos_pily)
    	if self.__minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia is not None:
    		self.__cmd = "%s --param8=\"%s\"" % (self.__cmd, self.__ilosc_ciec_na_pasie)

    	if self.__max_layout_size is not None:
    		self.__cmd = "%s --param55=\"%s\"" % (self.__cmd, self.__max_layout_size)
    	if self.__vertical_cut_direction is not None:
    		self.__cmd = "%s --param66=\"%s\"" % (self.__cmd, self.__vertical_cut_direction)
    	if self.__horizontal_cut_direction is not None:
    		self.__cmd = "%s --param77=\"%s\"" % (self.__cmd, self.__horizontal_cut_direction)
    	if self.__auto_cut_direction is not None:
    		self.__cmd = "%s --param88=\"%s\"" % (self.__cmd, self.__auto_cut_direction)

        # obraz
    	if self.__sciezka_do_obrazu is not None:
    		self.__cmd = "%s --param100=\"%s\"" % (self.__cmd, self.__sciezka_do_obrazu)

    	# ustawienia dla socket
    	self.__cmd = "%s --param99=\"%s\"" % (self.__cmd, self.__socket_port)

    	return self.__cmd

    def generuj_dane(self):
    	self.__port_do_komunikacji(self.s.PORT)

    	import threading
    	thread = threading.Thread(target=self.s.talking)  # w tle
    	thread.daemon = True
    	thread.start()

    	#self.__port_do_komunikacji(s.PORT)
    	self.__dodaj_parametry_do_zapytania()

    	import subprocess
    	p = subprocess.call(("""%s""") % self.__cmd, shell=True)

    	data_json = self.s.data

    	#if data_json == None:
    	    #return None
    	#    raise ValueError('Najprawdopodobniej jest podanych za mało arkuszy dla formatek')
    	#else:
    	print('\n')
    	print("Dane z optymalizatora przed przeksztalceniem w obiekt python ", data_json, data_json is None, type(data_json))

    	self.Agregator_Argumentow = Agregator_Argumentow(data_json)

    	print('\n')
    	print("Dane z optymalizatora po przeksztalceniu z json na obiekt python ", self.Agregator_Argumentow.dane)
    	return True


    ###########
    # DLA PYRO
    ###########
    def zamknij_polaczenie(self):
        self.s.keep_talking = False  # wylaczam threadingparam4
        self.s.sock.shutdown(1)
        self.s.sock.close()

    def ilosc_ciec(self):
        return self.Agregator_Argumentow.ilosc_ciec()

    def pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy(self):
        return self.Agregator_Argumentow.pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy()

    def pokaz_ciecia_dla_konkretnego_arkusza_bez_trim(self, sheet_nr):
        return self.Agregator_Argumentow.pokaz_ciecia_dla_konkretnego_arkusza_bez_trim(sheet_nr)

    def pokaz_ilosc_ciec_bez_trim_dla_wszystkich_arkuszy(self):
        return self.Agregator_Argumentow.pokaz_ilosc_ciec_bez_trim_dla_wszystkich_arkuszy()

    def pokaz_arkusze_ktore_nie_zostana_pociete(self):
        return self.Agregator_Argumentow.pokaz_arkusze_ktore_nie_zostana_pociete()

    def pokaz_sheet_nr_arkuszy_ktore_zostana_pociete(self):
        return self.Agregator_Argumentow.pokaz_sheet_nr_arkuszy_ktore_zostana_pociete()

    def pokaz_ilosc_arkuszy_do_pociecia(self):
        return self.Agregator_Argumentow.pokaz_ilosc_arkuszy_do_pociecia()

    def pokaz_arkusze_ktore_zostana_pociete(self):
        return self.Agregator_Argumentow.pokaz_arkusze_ktore_zostana_pociete()

    def pokaz_arkusze_ktore_nie_zostana_pociete(self):
        return self.Agregator_Argumentow.pokaz_arkusze_ktore_nie_zostana_pociete()

    def pokaz_ciecia_dla_konkretnego_arkusza(self, sheet_nr):
        return self.Agregator_Argumentow.pokaz_ciecia_dla_konkretnego_arkusza(sheet_nr)

    def pokaz_ilosc_ciec_bez_trim_dla_konkretnego_arkusza(self, sheet_nr):
        return self.Agregator_Argumentow.pokaz_ilosc_ciec_bez_trim_dla_konkretnego_arkusza(sheet_nr)

    def pokaz_arkusz(self, sheet_nr):
        return self.Agregator_Argumentow.pokaz_arkusz(sheet_nr)

    def pokaz_obrazy_dla_arkusza(self, sheet_nr):
        return self.Agregator_Argumentow.pokaz_obrazy_dla_arkusza(sheet_nr)

    def pokaz_formatki_arkusza(self, sheet_nr):
        return self.Agregator_Argumentow.pokaz_formatki_arkusza(sheet_nr)

    def pokaz_resztki_arkusza(self, sheet_nr):
        return self.Agregator_Argumentow.pokaz_resztki_arkusza(sheet_nr)
"""
a = Optymalizator()
a.dodaj_nr_zamowienia("76543456467")

# wartosci w mm
a.dodaj_arkusz(2000,1500,"9999")
a.dodaj_arkusz(2400,1500,"19999")
a.dodaj_arkusz(2300,1500,"29999")
a.dodaj_arkusz(2300,1500,"29999")
a.dodaj_arkusz(2300,1500,"29999")

# wartosci w mm
a.dodaj_formatke(900,800,"qqq",False)
a.dodaj_formatke(1900,800,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)
a.dodaj_formatke(900,700,"qqq",True)

a.obetnij_boki_arkusza_trim(20,20,20,20)  # wartosci w mm
a.definiuj_grubosc_pily(30)  # wartosci w mm
a.minimalizuj_ilosc_obrotow_arkusza_podczas_ciecia(True)
a.definiuj_ilosc_ciec_na_pasie(4)

a.generuj_dane()

print(a.Agregator_Argumentow.pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy())
print(a.Agregator_Argumentow.pokaz_ciecia_bez_trim_dla_wszystkich_arkuszy())
print(a.Agregator_Argumentow.pokaz_ilosc_ciec_bez_trim_dla_wszystkich_arkuszy())
print(a.Agregator_Argumentow.pokaz_arkusze_ktore_nie_zostana_pociete())
print(a.Agregator_Argumentow.pokaz_sheet_nr_arkuszy_ktore_zostana_pociete())
print(a.Agregator_Argumentow.pokaz_ilosc_arkuszy_do_pociecia())
print(a.Agregator_Argumentow.pokaz_arkusze_ktore_zostana_pociete())
print(a.Agregator_Argumentow.pokaz_arkusze_ktore_nie_zostana_pociete())
print(a.Agregator_Argumentow.pokaz_ciecia_dla_konkretnego_arkusza(1))  # sheet
print(a.Agregator_Argumentow.pokaz_arkusz(1))  # sheet
print(a.Agregator_Argumentow.pokaz_obrazy_dla_arkusza(1))  # sheet
print(a.Agregator_Argumentow.pokaz_formatki_arkusza(1))  # sheet
"""
