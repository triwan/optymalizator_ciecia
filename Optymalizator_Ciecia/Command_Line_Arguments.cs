// dla Arguments
using System;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Optymalizator_Ciecia
{
	public class Argument_Splitter{

		public static Material material_string_single(string data){
			/*
			 * Wykorzystuje w momencie dodawania nowego materialu (arkuszy), bez wielu wymiarow
			 * przypisuje argument z parametrami (np. "[1,2,3, nr_zamowienia]")
			 * do Material.Material
			 * Np. [1,2,3, sdsds] = [(double) wysokosc, (double) szerokosc,(int) ilosc, (string) nr_zamowienia]
			 *
			 */
			String[] substrings = data.Split ('&');  // ma byc 1, tutaj nie dzielimy bo nie ma &

			if (substrings.Length == 0)
			{
				throw new System.InvalidOperationException("Error. (material_string_single) Nie podano argumentow dla material arkusz");
			}
			else if (substrings.Length > 1)
			{
				throw new System.InvalidOperationException("Error. (material_string_single) Podano za dużo argumentów dla material arkusz");
			}
			else
			{
				// zawsze bedzie tylko 1 pozycja
				String dane_z_przecinkami = data.Replace ('[', ' ').Replace (']', ' ');  // usuwam [ i ]
				String[] dane = dane_z_przecinkami.Split (',');  // dziele 1,2,3, ne_zamowienia na 1 2 3 ne_zamowienia

				double wysokosc = Convert.ToDouble (dane [0]);
				double szerokosc = Convert.ToDouble (dane [1]);
				int ilosc = Convert.ToInt32 (dane [2]);
				string nr_zamowienia = dane [3];

				Material material = new Material (wysokosc,szerokosc, ilosc, nr_zamowienia);
				return material;
			}
		}

		public static Material material_string_many(string nr_zamowienia){
			/*
			 * Wykorzystuje w momencie dodawania nowego materialu (arkuszy), z wieloma wymiarami
			 * np. this.Material = new Material ("666");
			 *
			 */
			Material material = new Material (nr_zamowienia);
			return material;
		}

		public static Material formatka_string_list(string data, Material material){
			/*
			 * przypisuje argument z parametrami (np. "[1,2,3,true]&[4,5,6,true]&[7,8,9,false]")
			 * do Material.__Formatka_Part_Items
			 * Np. [1,2,3,true] = [(double) wysokosc, (double) szerokosc,(string) nr_formatkim, (bool) zachowac_strukture]
			 *
			 */

			//TODO sprawdzac poprawnosc data za pomoca wyrazen regularnych

			String[] substrings = data.Split ('&');  // dziele na [1,2,3,true]...
			foreach (var substring in substrings) {

				String dane_z_przecinkami = substring.Replace ('[', ' ').Replace (']', ' ');  // usuwam [ i ]
				String[] dane = dane_z_przecinkami.Split (',');  // dziele 1,2,3 na 1 2 3

				double wysokosc = Convert.ToDouble (dane [0]);
				double szerokosc = Convert.ToDouble (dane [1]);
				string nr_formatki = dane [2];
				bool zachowac_strukture = Convert.ToBoolean (dane [3]);

				material.formatka_dodaj (wysokosc, szerokosc, nr_formatki, zachowac_strukture);
			}

			return material;

		}


		public static Material arkusz_string_list(string data, Material material){
			/*
			 * Wykorzystuje jeśli w parametrze jest więcej niż jeden arkusz o roznych wymiarach
			 * przypisuje argument z parametrami (np. "[1,2,3]&[4,5,6]&[7,8,9]")
			 * do Material.__Arkusz_Stock_Items
			 * Np. [1,2,12345, 6, 7 (lub 0)] = [(double) wysokosc, (double) szerokosc,(int) (string) kod, id_towar (int), id_odpad_arkusz (int)]
			 * ilosc zawsze musi byc 1
			 *
			 */

			//TODO sprawdzac poprawnosc data za pomoca wyrazen regularnych


			String[] substrings = data.Split ('&');  // dziele na [1,2,3]...


			if (substrings.Length == 0)
			{
				throw new System.InvalidOperationException("Error. Nie podano argumentow dla material arkusz");
			}
			// jesli wiecej niz 1 arkusz i material jest zdefiniowany jako posiadajacy tylko arkusze tego samego rozmiaru
			else if (material.wiele_arkuszy == false)
			{
				throw new System.InvalidOperationException("Error. Nie mozna dodac argumentow z linii polecen. Do funkcji arkusz_string_list przeslano material ktory nie posiada wielu wymiarow arkuszy.");
			}
			else
			{
				foreach (var substring in substrings) {

					String dane_z_przecinkami = substring.Replace ('[', ' ').Replace (']', ' ');  // usuwam [ i ]
					String[] dane = dane_z_przecinkami.Split (',');  // dziele 1,2,12345 na 1 2 12345

					double wysokosc = Convert.ToDouble (dane [0]);
					double szerokosc = Convert.ToDouble (dane [1]);
					string kod = dane [2];
					int ilosc = 1; // dodajemy pojedynczo, wszystkie dane sa przypisywane bezposrednio do konretnego arkusza
					int id_towar = Convert.ToInt32(dane [3]);  
					int id_odpad_arkusz = Convert.ToInt32(dane [4]);  // jesli nie odpad podajemy 0

					material.arkusz_dodaj (wysokosc, szerokosc, ilosc, 0, kod, id_towar, id_odpad_arkusz);  //arkusz_dodaj(double wysokosc, double szerokosc, int ilosc, int sheet_nr=0, string kod="", int id_towar, int id_odpad_arkusz)
					//sheet_nr - jest wypelniany w momencie dodawania do kalkulatora
				}
			}

			return material;
		}

		public static double saw_width(string data)
		{
			/*
			 * uwzglednij przy obliczaniu grubosc pily
			 *
			 */

			double SawWidth = Convert.ToDouble (data);
			return SawWidth;
		}

		public static bool minimize_sheet_rotation(String data)
		{
			/*
			 * MinimizeSheetRotation
			 * jesli True - przy optymalizacji nie kalkuluje ciecia tak aby minimalizowac ilosci cie na resztkach
			 * bardziej wydajne, mniej obrotow ale resztki sa pociete
			 *
			 * jesli False - kalkuluje tak aby resztki byly jak najmniej pociete ale bedzie trzeba wiecej razy obrocic plyte
			 */
			bool MinimizeSheetRotation = Convert.ToBoolean (data);
			return MinimizeSheetRotation;
		}

		public static int max_cut_level(string data)
		{
			/*
			 * ilosc ciec na jednym z pasow
			 * wartosc od 2 do 6
			 * czym mniejsza wartosc tym mniej ciec ale mniej optymalne ciecia
			 * jesli wieksza wartosc to mozna pociac wiecej formatek ale przy wiekszej ilosci obrotow plyty
			 *
			 */
			int MaxCutLevel = Convert.ToInt32 (data);
			if (MaxCutLevel >= 2 && MaxCutLevel <= 6) {
				return MaxCutLevel;
			}
			else{
				throw new System.InvalidOperationException("Error. Zle wprowadzono MaxCutLevel --param8, mozna dodac poziom od 2 do 6");
			}

		}

		public static int max_layout_size(string data)
		{
			/*
			 * Definiuje ilosc plyt, ktore moga byc ciete naraz na pilarce. Dziala tylko jesli UseLayoutMinimization == true
			 */
			int MaxLayoutSize = Convert.ToInt32 (data);
			return MaxLayoutSize;
		}

		public static double waste_size_min(string data)
		{
			/*
			 * Minimal acceptable size of the waste parts (0 - no restrictions). It plays an important role when cut glass stocks 
			 * because it’s impossible to cut tiny pieces of glass. 
			 */
			double WasteSizeMin = Convert.ToDouble (data);
			return WasteSizeMin;
		}

		public static bool set_vertical_cut_direction(string data)
		{
			/*
			 * Suitable for roll (stripe) cutting. If aRoolMode is True then it ensures the first cut is made completely from the
			 * stock’s top to the bottom.
			 */
			bool SetVerticalCutDirection = Convert.ToBoolean (data);
			return SetVerticalCutDirection;
		}			

		public static bool set_horizontal_cut_direction(string data)
		{
			/*
			 * Suitable for roll (stripe) cutting. If aRoolMode is True then it ensures the first cut is made completely from the
			 * stock’s left side to the right one. 
			 */
			bool SetHorizontalCutDirection = Convert.ToBoolean (data);
			return SetHorizontalCutDirection;
		}			

		public static bool set_auto_cut_direction(string data)
		{
			/*
			 * Defines the calculation engine will automatically detect what direction produce better results and use this direction
			 * to cut the stock first time.
			 */
			bool SetAutoCutDirection = Convert.ToBoolean(data);
			return SetAutoCutDirection;
		}			

		public static Dictionary<string, double> trim_list(string data)
		{
			/*
			 * jesli maja byc wyorzystana opcja trim
			 * jesli arkusz jest nauchniety trzeba obciac go z kazdej strony
			 *
			 * [1,2,3,4] = [TrimLeft, TrimTop, TrimRight, TrimBottom]
			 */

			Dictionary<string, double> trim_data = new Dictionary<string, double> ();

			String dane_z_przecinkami = data.Replace ('[', ' ').Replace (']', ' ');  // usuwam [ i ]
			String[] dane = dane_z_przecinkami.Split (',');  // dziele 1,2,3,4 na 1 2 3 4


			trim_data.Add ("TrimLeft", Convert.ToDouble(dane [0]));
			trim_data.Add ("TrimTop", Convert.ToDouble(dane [1]));
			trim_data.Add ("TrimRight", Convert.ToDouble(dane [2]));
			trim_data.Add ("TrimBottom", Convert.ToDouble(dane [3]));

			return trim_data;
		}
	}

	public class Arguments{
		// Variables
		private StringDictionary Parameters;

		// Constructor
		public Arguments(string[] Args)
		{
			Parameters = new StringDictionary();
			Regex Spliter = new Regex(@"^-{1,2}|^/|=|:",
				RegexOptions.IgnoreCase|RegexOptions.Compiled);

			Regex Remover = new Regex(@"^['""]?(.*?)['""]?$",
				RegexOptions.IgnoreCase|RegexOptions.Compiled);

			string Parameter = null;
			string[] Parts;

			// Valid parameters forms:
			// {-,/,--}param{ ,=,:}((",')value(",'))
			// Examples:
			// -param1 value1 --param2 /param3:"Test-:-work"
			//   /param4=happy -param5 '--=nice=--'
			foreach(string Txt in Args)
			{
				// Look for new parameters (-,/ or --) and a
				// possible enclosed value (=,:)
				Parts = Spliter.Split(Txt,3);

				switch(Parts.Length){
				// Found a value (for the last parameter
				// found (space separator))
				case 1:
					if(Parameter != null)
					{
						if(!Parameters.ContainsKey(Parameter))
						{
							Parts[0] =
								Remover.Replace(Parts[0], "$1");

							Parameters.Add(Parameter, Parts[0]);
						}
						Parameter=null;
					}
					// else Error: no parameter waiting for a value (skipped)
					break;

					// Found just a parameter
				case 2:
					// The last parameter is still waiting.
					// With no value, set it to true.
					if(Parameter!=null)
					{
						if(!Parameters.ContainsKey(Parameter))
							Parameters.Add(Parameter, "true");
					}
					Parameter=Parts[1];
					break;

					// Parameter with enclosed value
				case 3:
					// The last parameter is still waiting.
					// With no value, set it to true.
					if(Parameter != null)
					{
						if(!Parameters.ContainsKey(Parameter))
							Parameters.Add(Parameter, "true");
					}

					Parameter = Parts[1];

					// Remove possible enclosing characters (",')
					if(!Parameters.ContainsKey(Parameter))
					{
						Parts[2] = Remover.Replace(Parts[2], "$1");
						Parameters.Add(Parameter, Parts[2]);
					}

					Parameter=null;
					break;
				}
			}
			// In case a parameter is still waiting
			if(Parameter != null)
			{
				if(!Parameters.ContainsKey(Parameter))
					Parameters.Add(Parameter, "true");
			}
		}

		// Retrieve a parameter value if it exists
		// (overriding C# indexer property)
		public string this [string Param]
		{
			get
			{
				return(Parameters[Param]);
			}
		}
	}
}
