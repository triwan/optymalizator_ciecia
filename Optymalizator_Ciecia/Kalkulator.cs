﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CutGLib;		

namespace Optymalizator_Ciecia
{	

	public class Kalkulator
	{		
		
		public Material material_cache = new Material();

		public Kalkulator ()
		{
		}
			
		// This rotine outputs the results for 2D cutting optimization
		private static List<Dictionary<string, string>> OutputSheetResults_by_Parts(CutEngine aCalculator)
		{
			List<Dictionary<string, string>> lista_danych_cut = new List<Dictionary<string, string>>();  // ciecia dla formatek


			int StockNo, iCut, iPart;
			long CutsCount;
			double Width, Height, X1 = 0, Y1 = 0, X2 = 0, Y2 = 0;
			bool active;
			string id;
			Console.Write("\n");
			Console.Write("OUTPUT CUTS RESULTS\n");
			Console.Write("Used {0} sheets\n", aCalculator.UsedStockCount);
			// Output guilltoine cuts for each sheet
			for (StockNo = 0; StockNo < aCalculator.StockCount; StockNo++)
			{
				aCalculator.GetStockInfo(StockNo, out Width, out Height, out active);
				// Sheet was not used during calculation
				if (!active)
				{
					Console.Write("Sheet={0} was not used.\n", StockNo);
					continue;
				}
				Console.Write("Sheet={0}: Width={1} Height={2}\n", StockNo, Width, Height);
				// First output any trim cuts for the sheet StockNo
				CutsCount = aCalculator.GetStockTrimCutCount(StockNo);
				for (iCut = 0; iCut < CutsCount; iCut++)
				{
					aCalculator.GetStockTrimCut(StockNo, iCut, out X1, out Y1, out X2, out Y2);
					Console.Write("Trim  Cut={0}:  X1={1};  Y1={2};  X2={3};  Y2={4}\n", iCut, X1, Y1, X2, Y2);


					Dictionary<string, string> dane_trim_cut = new Dictionary<string, string> ();

					dane_trim_cut.Add ("trim", "true");  // ciecia (trim) - nieuzytek (wszystkie boki pociete w momencie jesli napuchnieta plyta), 
					//trim - klient za to nie placi
					dane_trim_cut.Add ("sheet", StockNo.ToString());
					dane_trim_cut.Add ("cut", iCut.ToString());
					dane_trim_cut.Add ("X1", X1.ToString());
					dane_trim_cut.Add ("Y1", Y1.ToString());
					dane_trim_cut.Add ("X2", X2.ToString());
					dane_trim_cut.Add ("Y2", Y2.ToString());

					lista_danych_cut.Add (dane_trim_cut);
				}
				// Now output any actual cuts for the sheet StockNo
				CutsCount = aCalculator.GetStockCutCount(StockNo);
				for (iCut = 0; iCut < CutsCount; iCut++)
				{
					aCalculator.GetStockCut(StockNo, iCut, out X1, out Y1, out X2, out Y2);
					Console.Write("Cut={0}:  X1={1};  Y1={2};  X2={3};  Y2={4}\n", iCut, X1, Y1, X2, Y2);

					Dictionary<string, string> dane_cut = new Dictionary<string, string> ();
					dane_cut.Add ("trim", "false");
					dane_cut.Add ("sheet", StockNo.ToString());
					dane_cut.Add ("cut", iCut.ToString());
					dane_cut.Add ("X1", X1.ToString());
					dane_cut.Add ("Y1", Y1.ToString());
					dane_cut.Add ("X2", X2.ToString());
					dane_cut.Add ("Y2", Y2.ToString());

					lista_danych_cut.Add (dane_cut);
				}
			}

			// Get parts locations
			double W = 0, H = 0, X = 0, Y = 0;
			bool Rotated;
			Console.Write("\n");
			Console.Write("OUTPUT PARTS RESULTS");
			Console.Write("\nPart Count={0}\n", aCalculator.PartCount);
			for (iPart = 0; iPart < aCalculator.PartCount; iPart++)
			{
				// Get sizes and location of the source part with index Iter
				// in case of incomplete optimization the part can be unplaced
				// and the function returns FALSE.
				if (aCalculator.GetResultPart(iPart, out StockNo, out W, out H, out X, out Y, out Rotated, out id))
				{
					Console.Write("Part ID={0};  sheet={1};  X={2};  Y={3};  Width={4};  Height={5}\n",
						id, StockNo, X, Y, W, H);
				}
				else Console.Write("Part {0} was not placed\n", iPart);
			}
			Console.Write("\n");
			Console.WriteLine ("END====================================\n");

			return lista_danych_cut;
		}

		// This rotine outputs the results for 2D cutting optimization by layouts
		private static List<Dictionary<string, string>> OutputSheetResults_by_Layout(CutEngine aCalculator)
		{
			int ilosc_wykorzystanych_arkuszy = aCalculator.UsedStockCount;  // dane
			List<Dictionary<string, string>> lista_danych_layout = new List<Dictionary<string, string>>();  // zwracam - slownik w liscie



			int sheetIndex,StockCount,iPart,iLayout,partCount,partIndex,tmp,iSheet;
			double width,height,X,Y,W,H;
			bool rotated,sheetActive;
			string Txt;
			Console.Write("\n");
			Console.WriteLine ("Kalkulator.cs============================");
			Console.WriteLine ("Wynik z CutGlib (kalkulator) :\n");
			Console.Write("OUTPUT LAYOUT RESULTS\n");
			Console.Write("Used {0} sheets\n", aCalculator.UsedStockCount);
			Console.Write("Created {0} different layouts\n", aCalculator.LayoutCount);

			// Iterate by each layout and output information about each layout,
			// such as number and length of used stocks and part indices cut from the stocks
			for (iLayout = 0; iLayout < aCalculator.LayoutCount; iLayout++)
			{

				aCalculator.GetLayoutInfo(iLayout, out sheetIndex, out StockCount);
				// sheetIndex is global index of the first sheet used in the layout iLayout
				// StockCount is quantity of sheets of the same size as sheetIndex used for this layout


				if (StockCount > 0)
				{
					Console.Write("Layout={0}:  Start Sheet={1};  Count of Sheet={2}\n", iLayout, sheetIndex, StockCount);
					// Output information about each stock, such as stock Length
					for (iSheet = sheetIndex; iSheet < sheetIndex + StockCount; iSheet++)
					{
						

						if (aCalculator.GetStockInfo(iSheet, out width, out height, out sheetActive))
						{
							//dane_layout.Add("sheet", iSheet.ToString());
							Dictionary<string, string> dane_layout = new Dictionary<string, string> ();

							Console.Write("Sheet={0}:  Width={1}; Height={2}\n", iSheet, width, height);
							// Output the information about parts cut from this sheet
							// First we get quantity of parts cut from the sheet
							partCount = aCalculator.GetPartCountOnStock(iSheet);
							// Iterate by parts and get indices of cut parts
							for (iPart = 0; iPart < partCount; iPart++)
							{
								// Get global part index of iPart cut from the current sheet
								partIndex = aCalculator.GetPartIndexOnStock(iSheet, iPart);
								// Get sizes and location of the source part with index partIndex
								if (aCalculator.GetResultPart(partIndex, out tmp, out W, out H, out X, out Y, out rotated))
								{
									// Output the coordinates
									if (rotated) Txt = "Yes";
									else Txt = "No";
									Console.Write("Part={0}; sheet={1}; Width={2}; Height={3}; X={4}; Y={5}; Rotated={6}\n",
										partIndex, iSheet, W, H, X, Y, Txt);

									dane_layout = new Dictionary<string, string> ();

									dane_layout.Add("sheet_width", width.ToString());
									dane_layout.Add("sheet_height", height.ToString());
									dane_layout.Add("part", partIndex.ToString());
									dane_layout.Add("sheet", iSheet.ToString());
									dane_layout.Add ("part_width", W.ToString());
									dane_layout.Add("part_height", H.ToString());
									dane_layout.Add("X", X.ToString());
									dane_layout.Add("Y", Y.ToString());
									dane_layout.Add("rotated", rotated.ToString());
									dane_layout.Add("ilosc_wykorzystanych_w_sumie_arkuszy", ilosc_wykorzystanych_arkuszy.ToString());
									dane_layout.Add("ilosc_resztek_z_arkusza", aCalculator.GetRemainingPartCountOnStock (iSheet).ToString());
								}
								lista_danych_layout.Add (dane_layout);
							}

						}
							
					}

				}

			}
			return lista_danych_layout;
			Console.Write("\n");
		}

		// Outputs the layout information.
		private static void OuputLayoutInfo(CutEngine aCalculator)
		{
			int StockNo, StockCount;

			Console.Write("Used {0} sheets\n", aCalculator.UsedStockCount);
			Console.Write("Created {0} different layouts\n", aCalculator.LayoutCount);
			for (int iLayout = 0; iLayout < aCalculator.LayoutCount; iLayout++)
			{
				aCalculator.GetLayoutInfo(iLayout, out StockNo, out StockCount);
				if (StockCount > 0)
				{
					Console.Write("Layout={0}:  Start Sheet={1};  Count of Sheets={2}\n", iLayout, StockNo, StockCount);
				}
			}
		}
			

		public CutEngine SetCalculator(		
										Dictionary<string, double> trim, 
										double saw_width,
										bool minimize_sheet_rotation,
										int max_cut_level,
										double waste_size_min,
										int max_layout_size,
										bool set_vertical_cut_direction,
										bool set_horizontal_cut_direction,
										bool set_auto_cut_direction
									  )
		{
			/*
			 * Ustawia kalkulator
			 */
			CutEngine Calculator = new CutEngine();

			// ustawiam trim
			if (trim.Count == 4) {
				Calculator.TrimLeft = trim["TrimLeft"];
				Calculator.TrimTop = trim["TrimTop"];
				Calculator.TrimRight = trim["TrimRight"];
				Calculator.TrimBottom = trim["TrimBottom"];
			}

			// ustawiam grubosc pily
			Calculator.SawWidth = saw_width; // jesli nie podano to == 0

			// ustawiam MinimizeSheetRotation; - True (domyslnie) mniej obrotow, mniej optymalnie pociete resztki
			Calculator.MinimizeSheetRotation = minimize_sheet_rotation;

			// ustawiam MaxCutLevel = domyslnie 6 - czym mniej tym mniej obrotow ale mniej formatek z arkusza
			Calculator.MaxCutLevel = max_cut_level;

			// ustawiam waste_size_min = jesli 0 to zostawiam domyslnie
			if (waste_size_min > 0)
			{
				Calculator.WasteSizeMin = waste_size_min;
			}


			// ustawiam max_layout_size = jesli <= 1 to zostawiam domyslnie
			if (max_layout_size > 1)
			{
				Calculator.MaxLayoutSize = max_layout_size;
			}				

			// ustawiam set_vertical_cut_direction = jesli True to set_vertical_cut_direction musi byc false
			if (set_vertical_cut_direction == true)
			{
				if (set_vertical_cut_direction == true && set_horizontal_cut_direction == true)
					throw new System.InvalidOperationException ("Error. Nie mozna jedsnoczesnie wywolac funkcji set_vertical_cut_direction == True i set_horizontal_cut_direction == True");
				else
					Calculator.SetVerticalCutDirection(set_vertical_cut_direction);
			}

			// ustawiam set_horizontal_cut_direction = jesli True to set_horizontal_cut_direction musi byc false
			if (set_horizontal_cut_direction == true)
			{
				if (set_vertical_cut_direction == true && set_horizontal_cut_direction == true)
					throw new System.InvalidOperationException ("Error. Nie mozna jedsnoczesnie wywolac funkcji set_vertical_cut_direction == True i set_horizontal_cut_direction == True");
				else
					Calculator.SetHorizontalCutDirection(set_horizontal_cut_direction);
			}

			// ustawiam set_auto_cut_direction = jesli True to set_horizontal_cut_direction i set_vertical_cut_direction musi byc false
			if (set_auto_cut_direction == true)
			{
				if (set_vertical_cut_direction == true || set_horizontal_cut_direction == true)
					throw new System.InvalidOperationException ("Error. Nie mozna wywolac funkcji set_vertical_cut_direction == True lub set_horizontal_cut_direction == True w momencie jesli chcemy ustawic SetAutoCutDirection");
				else
					Calculator.SetAutoCutDirection ();
			}



			// wyswietl informacje
			Console.WriteLine ("Kalkulator.cs============================");
			Console.WriteLine ("Ustawienia kalkulator :");

			Console.WriteLine ("Trim :\n");
			Console.WriteLine ("TrimLeft - {0}", Calculator.TrimLeft);
			Console.WriteLine ("TrimTop - {0}", Calculator.TrimTop);
			Console.WriteLine ("TrimRight - {0}", Calculator.TrimRight);
			Console.WriteLine ("TrimBottom - {0}", Calculator.TrimBottom);

			Console.WriteLine ("\nSawWidth - {0}", Calculator.SawWidth);
			Console.WriteLine ("\nMinimizeSheetRotation - {0}", Calculator.MinimizeSheetRotation);
			Console.WriteLine ("\nMaxCutLevel - {0}", Calculator.MaxCutLevel);

			Console.WriteLine ("\nWasteSizeMin - {0}", waste_size_min);
			Console.WriteLine ("\nSetVerticalCutDirection - {0}", set_vertical_cut_direction);
			Console.WriteLine ("\nSetHorizontalCutDirection - {0}", set_horizontal_cut_direction);
			Console.WriteLine ("\nSetAutoCutDirection - {0}", set_auto_cut_direction);
			Console.WriteLine ("END====================================\n");

			
			return Calculator;

		}

		public CutEngine SheetMultipleSheetSize(
												Material material, 
												String nr_zamowienia, 
												Dictionary<string, double> trim, 
												double saw_width,
												bool minimize_sheet_rotation,
												int max_cut_level,
												double waste_size_min,
												int max_layout_size,
												bool set_vertical_cut_direction,
												bool set_horizontal_cut_direction,
												bool set_auto_cut_direction
												)
		{
			/*
			 * funkcje wykorzystywac tylko jesli arkusze sa w roznych wymiarach
			 * 
			 * Dodawanie elementow do kalkolatora (CutGlib) z Material
			 * Ustawienie trim jesli istnieje 
			 * Ustawienie SawWidth - grubosc pily
			*/

			CutEngine Calculator = SetCalculator(trim, saw_width, minimize_sheet_rotation, max_cut_level, waste_size_min, max_layout_size,
													set_vertical_cut_direction, set_horizontal_cut_direction, set_auto_cut_direction); // nowa instancja

			Material material_temp = new Material (nr_zamowienia);

			// dodaje do obliczen arkusze o roznych rozmiarach
			int sheet_nr = 0;  // nr arkusza, do pozniejszego scalenia z danymi z kalkulatora
			foreach (var arkusz_items in material.arkusz_lista()) {
				// bool AddStock(double aWidth, double aHeight, int aCount, string aID) 
				//cCalculator.AddStock(arkusz_items.szerokosc, arkusz_items.wysokosc, arkusz_items.ilosc, arkusz_items.nr_zamowienia);  

				Calculator.AddStock(arkusz_items.szerokosc, arkusz_items.wysokosc, arkusz_items.ilosc, arkusz_items.nr_zamowienia);

				material_temp.arkusz_dodaj (arkusz_items.wysokosc, arkusz_items.szerokosc, arkusz_items.ilosc, 
					sheet_nr, arkusz_items.kod, arkusz_items.id_towar, arkusz_items.id_odpad_arkusz);

				sheet_nr++;
			}
				
		
			// dodaje do obliczen formatki
			int part_nr = 0; // nr formatki, do pozniejszego scalenia z danymi z kalkulatora
			foreach (var formatka_items in material.formatka_lista()) {
				// bool AddPart(double aWidth, double aHeight, int aCount, bool aRotatable, string aID)
				// !formatka_items.zachowac_strukture = przeciwne bool

				Calculator.AddPart(formatka_items.szerokosc, formatka_items.wysokosc, 
					formatka_items.ilosc(), !formatka_items.zachowac_strukture, formatka_items.opis_wymiar()); 

				material_temp.formatka_dodaj (formatka_items.wysokosc, formatka_items.szerokosc, 
												formatka_items.nr_formatki, !formatka_items.zachowac_strukture, part_nr);
				part_nr++;
			}				
		

			// kalkuluje dane
			string result = Calculator.Execute();

			this.material_cache = material_temp;

			// zamienia dane na ciag znakow przed przekazaniem do programu nadrzednego
			// Agregator_Danych.Agreguj_material (material_temp);

			return Calculator;
		}

		public static List<Dictionary<string, string>> wywolaj_OutputSheetResults_by_Layout(CutEngine calculator)
		{
			
			return OutputSheetResults_by_Layout(calculator); 
		}


		public static List<Dictionary<string, string>> wywolaj_OutputSheetResults_by_Parts(CutEngine calculator)
		{

			return OutputSheetResults_by_Parts(calculator); 
		}

		public static String utworz_obraz(CutEngine calculator, List<int> lista_arkuszy_do_wykorzystania, string kod, 
											string nr_zamowienia, string sciezka)
		{
			/*
			 * lista_arkuszy_do_wykorzystania - nie wszystkie arkusze beda wykorzystane
			 * generuje obraz tylko dla tych co beda ciete
			*/
			Console.WriteLine ("Kalkulator.cs============================");
			Console.WriteLine ("utworz_obraz (kalkulator) :");

			// string imageFile = System.Reflection.Assembly.GetExecutingAssembly().Location;  // sciezka do katalogu programu
			string imageFile = sciezka;
			const string obraz_results_format = "{{\"sheet\":{0}, \"plik\":\"{1}\"}}";   // format informacji zrotnej
			string callback = "";

			foreach( var sheet_nr in lista_arkuszy_do_wykorzystania)
			{
				try
				{
					//if (uzyte_arkusze.Exists (element => element == arkusz_items.sheet_nr)) {
					string image_name = String.Format ("{0}-{1}-{2}.png", nr_zamowienia, sheet_nr, kod);

					imageFile = Path.Combine (Path.GetDirectoryName (imageFile), image_name);

					// if the location of the executable file is read-only then we'll get exception here
					calculator.CreateStockImage (sheet_nr, imageFile, 1000);

					//dodaje do informacji zwrotnej
					callback += string.Format(obraz_results_format, 
												sheet_nr, 												
												image_name);  // musze zamienic z Replace("\\", "/") poniewaz json sobie nie radzil 						
					callback += ",";

					Console.WriteLine ("Nr arkusza - {0}", sheet_nr);
					Console.WriteLine ("Sciezka do obrazu {0}\n", imageFile);
	
				}
				catch (Exception e){
					throw new System.InvalidOperationException("Error. Nieprawidlowo wygenerowany plik obrazu - {0}", e);
				}
			}

			//calculator.ExportToCSV ("test.csv");
			//calculator.ExportToDXF ("1111.dxf", true);

			callback = callback.Remove(callback.Length - 1);  // usuwa ostatni znak - ,
			Console.WriteLine ("END====================================\n");

			return callback;
		}
	}
}

