﻿using System;
using System.Collections.Generic;

namespace Optymalizator_Ciecia
{
	public struct Arkusz_Stock
	{
		public int sheet_nr;  // nr arkusza, nr jest dodawany co 1 w zaleznosci od kolejnosci dodawania do kalkulatora.
		// sheet_nr potrzebne do scalenia informacji z kalkulatora i danymi Arkusz_Stock
		public double wysokosc;  //wyzsza wartosc arkusza (mm)
		public double szerokosc;  // nizsza wartosc (mm)
		public int ilosc;
		public string nr_zamowienia;  // opis (aID - text)
		public string kod;  // Towar.kod, mozna opisac przed przyjsciem na pile arkusz
		public int id_towar;
		public int id_odpad_arkusz;
		// pracownik na pile odczyta kod i wyswietli mu sie odpowiedni rozkroj dla danego arkusza
		// jesli bedziemy miec do czynienia z odpadem to wtedy kod = Magazyn.odpad_arkusz.kod_odpadu
		// nazwa pliku z rozkrojem == kod towaru
		// kod mozna tylko dodac jesli dodajemy arkusze o roznych wymiarach i po 1 sztuce
	}

	public struct Formatka_Part
	{
		public int sheet_nr;  // nr arkusza, powiazany z kalkulatorem i arkuszem
		public int part_nr; // nr formatki, nr jest dodawany co 1 w zaleznosci od kolejnosci dodawania do kalkulatora.
		// part_nr potrzebne do scalenia informacji z kalkulatora i danymi Formatka_Part
		public double wysokosc;
		public double szerokosc;
		public string nr_formatki;
		public string nr_zamowienia;
		public bool zachowac_strukture;  // dla kalkulatora oznaczamy czy moze obracac przy obliczeniach
		// jesli zachowac_strukture == True to oblicza bez obracania

		public int ilosc()
		{
			// dodajemy zawsze 1 poniewaz musimy miec unikalne opisy dla kazdej z formatek
			return 1;
		}

		public string opis_wymiar()
		{
			return this.wysokosc + "x" + this.szerokosc;
		}

	}

	public class Material
	{
		/*
		 *  DLA JEDNEGO WYMIARU ARKUSZY
		 *
		 	this.Material = new Material (10, 20, 5, "666");
			this.Material.formatka_dodaj (110, 220, "1666", true);
			this.Material.formatka_dodaj (9110, 9220, "91666", false);
			this.Material.formatka_dodaj (810, 820, "866", true);

			var arkusz = this.Material.arkusz();
			Console.WriteLine("Arkusz - wysokosc {0}, szerokosc {1}, ilosc {2}, nr namowienia {3} ",
				arkusz.wysokosc, arkusz.szerokosc, arkusz.ilosc, arkusz.nr_zamowienia);

			foreach (var formatka_items in this.Material.formatka_lista()) {
				Console.WriteLine("Formatka - wysokosc {0}, szerokosc {1}, nr_formatki {2}, nr namowienia {3}, zachowac strukture {4}",
					formatka_items.wysokosc, formatka_items.szerokosc,
					formatka_items.nr_formatki, formatka_items.nr_zamowienia, formatka_items.zachowac_strukture);
		*/

		/*
		*
		*	DLA WIELU WYMIARÓW ARKUSZY
		*
			this.Material = new Material ("666");
			this.Material.arkusz_dodaj (2400, 2000, 2);
			this.Material.arkusz_dodaj (2500, 2070, 1, 6667778);

			this.Material.formatka_dodaj (110, 220, "1666", true);
			this.Material.formatka_dodaj (9110, 9220, "91666", false);
			this.Material.formatka_dodaj (810, 820, "866", true);

			foreach (var arkusz_items in this.Material.arkusz_lista()) {
				Console.WriteLine ("Arkusz - wysokosc {0}, szerokosc {1}, ilosc {2}, nr namowienia {3}, kod {4} ",
					arkusz_items.wysokosc, arkusz_items.szerokosc, arkusz_items.ilosc, arkusz_items.nr_zamowienia, arkusz_items.kod);
			}

			foreach (var formatka_items in this.Material.formatka_lista()) {
				Console.WriteLine("Formatka - wysokosc {0}, szerokosc {1}, nr_formatki {2}, nr namowienia {3}, zachowac strukture {4}",
					formatka_items.wysokosc, formatka_items.szerokosc,
					formatka_items.nr_formatki, formatka_items.nr_zamowienia, formatka_items.zachowac_strukture);
			}
		*/

		public bool wiele_arkuszy;  // oznacze zeby wiedziec z czym mam do czynienia
		private List<Formatka_Part> __Formatka_Part_Items = new List<Formatka_Part>();
		private List<Arkusz_Stock> __Arkusz_Stock_Items = new List<Arkusz_Stock>();  // dla wielu wymiarow arkuszy
		private Arkusz_Stock __Arkusz;  // dla jednego wyamiru arkuszy
		string nr;

		public Material(){
		}

		public Material (string nr_zamowienia)
		{
			// w tym kontruktorze definiujemy wiele wymiarów arkuszy
			this.wiele_arkuszy = true;

			this.nr = nr_zamowienia;
		}

		public Material (double wysokosc, double szerokosc, int ilosc, string nr_zamowienia)
		{
			// w tym kontruktorze z gory definiujemy jeden wymiar arkuszy
			this.wiele_arkuszy = false;

			__Arkusz.wysokosc = wysokosc;
			__Arkusz.szerokosc = szerokosc;
			__Arkusz.ilosc = ilosc;
			__Arkusz.nr_zamowienia = nr_zamowienia;
			this.nr = nr_zamowienia;
		}

		public Arkusz_Stock arkusz()
		{
			// dla jednego wymiaru arkuszy
			if (this.wiele_arkuszy == false) {
				return __Arkusz;
			} else {
				Console.Write (this.wiele_arkuszy);
				throw new System.InvalidOperationException("Error. Przy inicjalizacji wybrano Material ktory posiada wiele wymiarow arkuszy. Funkcja arkusz zwraca tylko jeden wymiar arkuszy");
			}

		}

		public List<Arkusz_Stock> arkusz_lista()
		{
			// dla wielu wymiarow arkuszy
			if (this.wiele_arkuszy == true) {
				return __Arkusz_Stock_Items;
			} else {
				throw new System.InvalidOperationException("Error. Przy inicjalizacji wybrano Material ktory posiada tylko jeden wymiar arkuszy. Funkcja arkusz_lista zwraca wiele arkuszy o roznych wymiarach");
			}
		}

		public void arkusz_dodaj(double wysokosc, double szerokosc, int ilosc, int sheet_nr, string kod, int id_towar, int id_odpad_arkusz)
		{
			// dla wielu wymiarow arkuszy
			if (this.wiele_arkuszy == true) {

				if (kod.Length > 0 && ilosc > 1)
				{
					throw new System.InvalidOperationException ("Error. Jeśli ma zostać dodany kod do arkusza, dodaj arkusze po 1 sztuce z konkretnym kodem");
				}
				else
				{
					var arkusz = new Arkusz_Stock ();

					arkusz.sheet_nr = sheet_nr;

					arkusz.wysokosc = wysokosc;
					arkusz.szerokosc = szerokosc;
					arkusz.ilosc = ilosc;
					arkusz.nr_zamowienia = this.nr;
					arkusz.kod = kod;  // tylko jesli podajemy po 1 sztuce
					arkusz.id_towar = id_towar;
					arkusz.id_odpad_arkusz = id_odpad_arkusz;
					__Arkusz_Stock_Items.Add (arkusz);
				}
			} else {
				throw new System.InvalidOperationException("Error. Przy inicjalizacji wybrano Material ktory posiada tylko jeden wymiar arkuszy. Funkcja arkusz_dodaj dodaje wiele arkuszy o roznych wymiarach");
			}

		}

		public List<Formatka_Part> formatka_lista()
		{
			return __Formatka_Part_Items;
		}

		public void formatka_dodaj(double wysokosc, double szerokosc, string nr_formatki, bool zachowac_strukture, int part_nr=0)
		{
			var formatka = new Formatka_Part();
			formatka.wysokosc = wysokosc;
			formatka.szerokosc = szerokosc;
			formatka.nr_formatki = nr_formatki;
			formatka.nr_zamowienia = this.nr;
			formatka.zachowac_strukture = zachowac_strukture;
			formatka.part_nr = part_nr;
			__Formatka_Part_Items.Add(formatka);
		}


		public List<int> uzyte_arkusze_podczas_ciecia()
		{
			/*
			 * zwraca liste uzytych arkuszy przy cieciu
			 * nie wszystkie arkusze sa wykorzystane
			 * funkcja dziala po uruchomieniu kalkulatora
			 */
			List<int> sheet_z_formatki = new List<int>(); // z formatek kopiuje sheet, unikalnie

			// sprawdza w formatce sheet i dodaje tylko unikalne
			foreach (var formatka_items in formatka_lista()) {
				bool exists = sheet_z_formatki.Exists (element => element == formatka_items.sheet_nr);
				if (exists == false) {
					sheet_z_formatki.Add (formatka_items.sheet_nr);
				}
			}
				
			return sheet_z_formatki;
				
		}

		public void wyswietl_wiele_rozmiarow()
		{
			Console.WriteLine ("Material.cs============================");
			Console.WriteLine ("Dodany material : ");
			foreach (var arkusz_items in arkusz_lista()) {
				Console.WriteLine ("Arkusz - wysokosc: {0}, szerokosc: {1}, ilosc: {2}, nr zamowienia: {3}, kod: {4}",
					arkusz_items.wysokosc, arkusz_items.szerokosc, arkusz_items.ilosc, arkusz_items.nr_zamowienia, arkusz_items.kod);
			}

			foreach (var formatka_items in formatka_lista()) {
				Console.WriteLine("Formatka - wysokosc: {0}, szerokosc: {1}, nr_formatki: {2}, nr zamowienia: {3}, zachowac strukture: {4}",
					formatka_items.wysokosc, formatka_items.szerokosc,
					formatka_items.nr_formatki, formatka_items.nr_zamowienia, formatka_items.zachowac_strukture);
			}
			Console.WriteLine ("END====================================\n");
		}
	}
}
